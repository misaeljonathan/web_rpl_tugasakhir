from django.apps import AppConfig


class DaftarTransaksiConfig(AppConfig):
    name = 'daftar_transaksi'
