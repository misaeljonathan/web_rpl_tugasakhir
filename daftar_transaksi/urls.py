from django.contrib import admin
from django.urls import path
from .views import index, tambah_transaksi

urlpatterns = [
    path('', index, name="hello-world"),
    path('tambah-transaksi', tambah_transaksi, name="tambah_transaksi")
]