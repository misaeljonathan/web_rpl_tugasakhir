from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Transaksi
from .forms import Transaksi_Form
from HalamanLogin.models import Pedagang

# Create your views here.
response = {}

def index(request):
    print("CHECK INDEX")
    if "username" in request.session:
        html = "index.html"
        response['transaksi_form'] = Transaksi_Form
        response['pedagang'] = Pedagang.objects.get(username=request.session["username"])
        transaksi = Transaksi.objects.filter(pedagang = response['pedagang'])
        # transaksi = Transaksi.objects.all()
        paginator = Paginator(transaksi, 5)
        page = request.GET.get('page', 1)
        try:
            listTransaksi = paginator.page(page)
        except PageNotAnInteger:
            listTransaksi = paginator.page(1)
        except EmptyPage:
            listTransaksi = paginator.page(paginator.num_pages)
        
        response['transaksi'] = transaksi

        return render(request, html, response)
    else :
        return HttpResponseRedirect('/halaman-login/')

def tambah_transaksi(request):
    form = Transaksi_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['idPemesanan'] = request.POST['idPemesanan']
        response['pemesan'] = request.POST['pemesan']
        response['makanan'] = request.POST['makanan']
        response['totalHarga'] = request.POST['totalHarga']
        response['status'] = request.POST['status']
        transaksi = Transaksi(pemesan=response['pemesan'],
            makanan=response['makanan'],
            idPemesanan=response['idPemesanan'],
            status=response['status'],
            totalHarga=response['totalHarga'],
            pedagang=Pedagang.objects.get(username=request.session["username"])
        )
        transaksi.save()

        return HttpResponseRedirect('/daftar-transaksi/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    index = data.number - 1
    max_index = len(paginator.page_range)

    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
