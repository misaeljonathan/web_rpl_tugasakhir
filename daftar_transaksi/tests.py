from django.test import TestCase
from django.test import Client
from HalamanLogin.models import Pedagang

# Create your tests here.
class DaftarTransaksiUnitTest(TestCase):

    # def test_daftar_transaksi_url_exist(self):
    #     response = Client().get('/daftar-transaksi/')
        
    def test_if_username_not_exist(self):
        response = Client().get('/daftar-transaksi/')
        self.assertEqual(response.status_code, 302)

    def test_if_username_exist(self):
        user = Pedagang.objects.create(username="Misael", password="Jonathan")
        response = self.client.post('/halaman-login/login/', {"username" : "Misael", "password" : "Jonathan"})
        print(response.client.session.items())
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/daftar-transaksi/')
        self.assertEqual(response.status_code, 200)
    
    # def test_if_redirect_to_daftar_transaksi_success(self):

