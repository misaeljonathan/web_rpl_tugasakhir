from django.db import models
from HalamanLogin.models import Pedagang

# Create your models here.
class Transaksi(models.Model):
    idPemesanan = models.PositiveIntegerField(default = 1)
    pemesan = models.CharField(max_length=30)
    totalHarga = models.PositiveIntegerField(default = 0)
    status = models.CharField(max_length=30)
    makanan = models.CharField(max_length=50)
    tanggal = models.DateField(auto_now=True)
    pedagang = models.ForeignKey(Pedagang, on_delete=models.CASCADE, null=True, blank=True)