from django import forms

class Transaksi_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    attrs = {
        'class': 'form-control'
    }

    pemesan_attr = {
        'class': 'form-control',
        'placeholder':'Nama Pemesan'
    }

    makanan_attr = {
        'class': 'form-control',
        'placeholder': 'List Makanan'
    }

    id_attr = {
        'class': 'form-control',
        'placeholder': 'ID Pemesan'
    }

    totalHarga_attr = {
        'class': 'form-control',
        'placeholder': 'Total Harga'    
    }

    status_attr = {
        'class': 'form-control',
        'placeholder': 'Status Pemesanan'    
    }

    pemesan = forms.CharField(label='Nama Pemesan', required=True, max_length=30, widget=forms.TextInput(attrs=pemesan_attr))
    makanan = forms.CharField(widget=forms.Textarea(attrs=makanan_attr), required=True)
    totalHarga = forms.CharField(label='Total Harga', required=True, max_length=30, widget=forms.TextInput(attrs=totalHarga_attr))
    status = forms.CharField(label='Status', required=True, max_length=30, widget=forms.TextInput(attrs=status_attr))
    idPemesanan = forms.CharField(label='ID Pemesanan', required=True, max_length=30, widget=forms.TextInput(attrs=id_attr))