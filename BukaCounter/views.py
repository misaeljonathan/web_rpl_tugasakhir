from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden
from .models import *
from HalamanLogin.models import *
from HalamanLogin.views import validate_user

# Create your views here.
response = {}

def index(request):
    if 'username' in request.session:
        uname = request.session.get('username')
        valid = validate_user(uname)
        if valid[1] == "pedagang":
            pedagang = Pedagang.objects.get(username=uname)
            if Counter.objects.filter(pedagang=pedagang).count() != 0:
                return HttpResponseForbidden()
        return render(request, "BukaCounter/BukaCounter.html", response)
    return HttpResponseForbidden()
    
def submit_form(request):
    if request.method == "POST":
        uname = request.session.get('username')
        pedagang = Pedagang.objects.get(username=uname)
        jenis = request.POST['jenisCounter']
        if jenis == "3":
            jenis = request.POST['jenisCounter2']
        Counter.objects.create(
            nama = request.POST['namaCounter'],
            jenis = jenis,
            jam_buka = request.POST['jamBukaCounter'],
            jam_tutup = request.POST['jamTutupCounter'],
            status_buka = request.POST['statusCounter'],
            pedagang = pedagang
        )
        print(Counter.objects.all().count())
    return redirect('/landing-page/daftar-menu')