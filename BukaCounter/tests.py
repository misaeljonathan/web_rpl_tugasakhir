from django.test import TestCase
from django.urls import resolve
from .views import *
from HalamanLogin.models import *
from .models import Counter

class BukaCounterUnitTest(TestCase):
    def setUp(self):
        self.pedagang = Pedagang.objects.create(username="user", password="pass")
        self.pedagang2 = Pedagang.objects.create(username="user2", password="pass2")
        Pembeli.objects.create(username="user3", password="pass3", saldo=1000)
        self.requestPost1 = {
            "namaCounter" : "asal",
            "jenisCounter" : "3",
            "jenisCounter2" : "asa",
            "jamBukaCounter" : "00:00",
            "jamTutupCounter" : "08:00",
            "statusCounter" : "Buka",
        }
        self.requestPost2 = {
            "namaCounter" : "asal",
            "jenisCounter" : "Makanan",
            "jamBukaCounter" : "00:00",
            "jamTutupCounter" : "08:00",
            "statusCounter" : "Buka",
        }

    def test_buka_counter_url_is_exist(self):
        response = self.client.get('/buka-counter/')
        self.assertEqual(response.status_code, 403)
        
        postForm = {"username":"user", "password":"pass"}
        self.client.post('/halaman-login/login/', postForm)
            
        response = self.client.get('/buka-counter/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_counter(self):
        new_counter = Counter.objects.create(
            nama = "asal",
            jenis = "Makanan",
            jam_buka = "00:00",
            jam_tutup = "08:00",
            status_buka = "Buka",
            pedagang = self.pedagang
        )
        counting_all_counter = Counter.objects.all().count()
        self.assertEqual(counting_all_counter, 1)

    def test_buka_counter_post1_success_and_access_again(self):
        postForm = {"username":"user", "password":"pass"}
        self.client.post('/halaman-login/login/', postForm)
        
        response_post = self.client.post('/buka-counter/submit-form/', self.requestPost1)
        self.assertEqual(response_post.status_code, 302)

        counting_all_counter = Counter.objects.all().count()
        self.assertEqual(counting_all_counter, 1)
            
        response = self.client.get('/buka-counter/')
        self.assertEqual(response.status_code, 403)

    def test_buka_counter_post2_success(self):
        postForm = {"username":"user2", "password":"pass2"}
        self.client.post('/halaman-login/login/', postForm)
        
        response_post = self.client.post('/buka-counter/submit-form/', self.requestPost2)
        self.assertEqual(response_post.status_code, 302)

        counting_all_counter = Counter.objects.all().count()
        self.assertEqual(counting_all_counter, 1)

