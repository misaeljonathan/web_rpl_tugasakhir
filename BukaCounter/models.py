from django.db import models
from HalamanLogin.models import *

# Create your models here.
class Counter(models.Model):
    nama = models.CharField(max_length=50)
    jenis = models.CharField(max_length=50)
    jam_buka = models.CharField(max_length=50)
    jam_tutup = models.CharField(max_length=50)
    status_buka = models.CharField(max_length=20)
    pedagang = models.OneToOneField(Pedagang, on_delete=models.CASCADE, null=True, blank=True)