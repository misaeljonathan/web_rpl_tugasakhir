$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      validate();
      event.preventDefault();
      return false;
    }
  });
});

$('#idPilihan').change(function(){
    if($('#idPilihan').val() == "3"){
        $('#idJenis').fadeIn();
    } else {
        $('#idJenis').fadeOut();
    }
});

function validate(){
    var patt = /[a-zA-Z]/;
    var isKosong = false;
    var inputs = $("form").serializeArray();
    var status = $("#idStatus").val();
    if(status == null){
        isKosong = true;
    }
    
    var inputan;
    var dateAwal;
    var dateAkhir;
    for (index = 0; index < inputs.length; ++index) {
        inputan = inputs[index];
        if(!(inputan["name"] == "jenisCounter" || inputan["name"] == "jenisCounter2") && inputan["value"] == ""){
            alert("Semua input tidak boleh kosong");
            return;
        }
        if(inputan["name"] == "namaCounter"){
            if(inputan["value"] == ""){
                alert("Semua input tidak boleh kosong");
                return;
            }
            if(! patt.test(inputan["value"])){
                alert("Nama Counter harus memiliki huruf");
                return;
            }
        }
        if(inputan["name"] == "jamBukaCounter"){
            dateAwal = parseInt(inputan["value"].replace(":", ""));
        }
        if(inputan["name"] == "jamTutupCounter"){
            dateAkhir = parseInt(inputan["value"].replace(":", ""));
        }
    }
    if($("#idPilihan").val() == null || ($("#idPilihan").val() == "3" && $("input[name=jenisCounter2]").val() == "") || $("#idStatus").val() == null){
        alert("Semua input tidak boleh kosong");
        return;
    }
    if (dateAkhir <= dateAwal){
        alert("Jam Tutup counter tidak boleh lebih dulu daripada Jam buka");
        return;
    }
    $("#SmokeModal").modal();
}