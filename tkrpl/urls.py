"""tkrpl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf import settings
from django.views.static import serve
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='/landing-page/')),
    path('landing-page/', include(("landing_page.urls", "landing_page"), 'landing_page')),
    path('ketersediaan-menu/', include(("KetersediaanMenu.urls", "KetersediaanMenu"), "KetersediaanMenu")),
    path('buka-counter/', include(("BukaCounter.urls", "BukaCounter"), 'BukaCounter')),
    path('halaman-login/', include(("HalamanLogin.urls", "HalamanLogin"), 'HalamanLogin')),
    path('tambah-menu/', include(("TambahMenu.urls", "TambahMenu"), 'TambahMenu')),
    path('daftar-transaksi/', include(("daftar_transaksi.urls", "daftar_transaksi"), 'daftar_transaksi')),
    path('media/<path>', serve, {'document_root' : settings.MEDIA_ROOT})
]
