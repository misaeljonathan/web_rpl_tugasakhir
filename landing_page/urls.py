from django.contrib import admin
from django.urls import path
from .views import index, daftar_menu

urlpatterns = [
    path('', index, name="hello-world"),
    path('daftar-menu', daftar_menu, name="daftar_menu")
]