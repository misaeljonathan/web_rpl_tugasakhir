from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden
from BukaCounter.models import *
from HalamanLogin.models import *
from HalamanLogin.views import validate_user
from TambahMenu.models import Menu

# Create your views here.
response = {}

def index(request):
    if not "username" in request.session:
        return redirect("/halaman-login/login/")
    return render(request, "landing_page/landing_page.html", {})

def daftar_menu(request):
    if ("username" in request.session) and (request.session["role"] == "pedagang"):
        if not Counter.objects.filter(pedagang=Pedagang.objects.get(username=request.session["username"])).exists():
            return redirect("/buka-counter/")
        
        html = "landing_page/daftar_menu.html"
        
        menuList = Menu.objects.filter(counter=Counter.objects.get(pedagang=Pedagang.objects.get(username=request.session["username"])))
        data = []
        
        for menu in menuList:
            data.append(
                {
                    "id" : menu.id,
                    "name" : menu.namaMenu,
                    "urlimage" : "/media/" + str(menu.fotoMenu)
                }
            )
        
        response["data"] = data
        
        return render(request, html, response)
    else:
        return HttpResponseForbidden()