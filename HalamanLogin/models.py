from django.db import models

class Users(models.Model):
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=128)
    
    def __str__(self):
        return self.username

    class Meta:
        abstract = True

class Admin(Users):
    pass

class Pedagang(Users):
    nama = models.CharField(max_length=50)
    no_ktp = models.CharField(max_length=30)
    no_telp = models.CharField(max_length=15)
    alamat = models.CharField(max_length=50)
    no_rek = models.CharField(max_length=20)

class Pembeli(Users):
    no_telp = models.CharField(max_length=15)
    saldo = models.IntegerField()
    foto = models.FileField(upload_to='images/', null=True, verbose_name="")