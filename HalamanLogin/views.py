from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Users, Admin, Pedagang, Pembeli

# Create your views here.
response = {}

def index(request):
    if "username" in request.session:
        return redirect("/landing-page/")
    html = "HalamanLogin/login.html"
    return render(request, html, response)

def validate_user(username):
    if(Pedagang.objects.filter(username=username).exists()):
        return (Pedagang.objects.get(username=username), "pedagang")
    elif(Pembeli.objects.filter(username=username).exists()):
        return (Pembeli.objects.get(username=username), "pembeli")
    elif(Admin.objects.filter(username=username).exists()):
        return (Admin.objects.get(username=username), "admin")
    else:
        return (None,None)

def auth_user(user, input_password):
    if(user.password==input_password):
        return True
    else:
        return False

def login(request):
    if request.method == "POST":
        valid = validate_user(request.POST['username'])
        if(valid[0]):
            auth = auth_user(valid[0], request.POST['password'])
            if(auth):
                request.session['username'] = request.POST['username']
                request.session['role'] = valid[1]
                return redirect('/landing-page/')
            else:
                messages.error(request, "Username atau Password salah")
                return redirect('/halaman-login/')
        else:
            messages.error(request, "Username atau Password salah")
            return redirect('/halaman-login/')
    else:
        return redirect('/halaman-login/')

def logout(request):
    if request.method == "POST":
        request.session.pop("username")
        request.session.pop("role")
        return redirect('/halaman-login/')
    return redirect('/landing-page/')
