from django.test import TestCase, Client
from .models import Admin,Pedagang,Pembeli
from optparse import OptionParser
import inspect

# Create your tests here.
class HalamanLoginTest(TestCase):

    def test_halaman_login_url_is_exist(self):
        response = Client().get('/halaman-login/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue("Login ke E-Canteen" in response.content.decode())

    def test_halaman_login_url_while_session(self):
        session = self.client.session
        session["username"] = "user"
        session.save()
        response = self.client.get('/halaman-login/')
        self.assertEqual(response.status_code, 302)

    def test_login_as_pedagang(self):
        user = Pedagang.objects.create(username='user',password='pass')
        response = Client().post('/halaman-login/login/', {'username' : 'user', 'password' : 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(str(user), "user")

    def test_login_as_pembeli(self):
        user = Pembeli.objects.create(username='user',password='pass',saldo=0)
        response = Client().post('/halaman-login/login/', {'username' : 'user', 'password' : 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(str(user), "user")

    def test_login_as_admin(self):
        user = Admin.objects.create(username='user',password='pass')
        response = Client().post('/halaman-login/login/', {'username' : 'user', 'password' : 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(str(user), "user")

    def test_login_user_not_found(self):
        response = Client().post('/halaman-login/login/', {'username' : 'user', 'password' : 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url , '/halaman-login/')

    def test_login_wrong_password(self):
        user = Pedagang.objects.create(username='user',password='password')
        response = Client().post('/halaman-login/login/', {'username' : 'user', 'password' : 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url , '/halaman-login/')

    def test_login_url_with_get(self):
        response = Client().get('/halaman-login/login/')
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        session = self.client.session
        session["username"] = "user"
        session["role"] = "role"
        session.save()
        response = self.client.post('/halaman-login/logout/',{'username' : session['username']})
        self.assertEqual(response.status_code, 302)
    
    def test_logout_with_get(self):
        response = Client().get('/halaman-login/logout/')
        self.assertEqual(response.status_code, 302)