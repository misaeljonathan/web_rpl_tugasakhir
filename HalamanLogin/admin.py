from django.contrib import admin
from .models import Admin, Pedagang, Pembeli

admin.site.register(Admin)
admin.site.register(Pedagang)
admin.site.register(Pembeli)