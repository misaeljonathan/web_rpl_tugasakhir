from django.contrib import admin
from django.urls import path
from .views import index, update

urlpatterns = [
    path('<id>/', index, name="ketersediaan_menu"),
    path('<id>/update/', update, name="update")
]