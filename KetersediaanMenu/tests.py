from django.test import TestCase
from HalamanLogin.models import *
from BukaCounter.models import Counter
from TambahMenu.models import Menu

# Create your tests here.

class KetersediaanMenuUnitTest(TestCase):
    def setUp(self):
        pedagang = Pedagang.objects.create(username="makan", password="makan")
        self.client.post('/halaman-login/login/', {"username":"makan", "password":"makan"})
        counter = Counter.objects.create(
            nama = "makan",
            jenis = "Makanan",
            jam_buka = "00:00",
            jam_tutup = "00:01",
            status_buka = "Buka",
            pedagang = pedagang
        )
        self.menu = Menu.objects.create(
            namaMenu = "makan",
            deksripsiMenu = "makan makan",
            fotoMenu = "makan.jpg",
            hargaMenu = 123123,
            counter = counter,
            status = "Ada"
        )

    def test_ketersediaan_menu_url_is_exist(self):
        response = self.client.get('/ketersediaan-menu/%d/' % (self.menu.id))
        self.assertEqual(response.status_code, 200)
    
    def test_ketersediaan_menu_url_is_not_exist(self):
        response = self.client.get('/ketersediaan-menu/%d/' % (self.menu.id + 1))
        self.assertEqual(response.status_code, 403)
    
    def test_update_success(self):
        response = self.client.post('/ketersediaan-menu/%d/update/' % (self.menu.id), {"status" : "Tidak Ada"})
        self.assertEqual(response.status_code, 200)
    
    def test_update_not_success(self):
        response = self.client.post('/ketersediaan-menu/%d/update/' % (self.menu.id + 1), {"status" : "Tidak Ada"})
        self.assertEqual(response.status_code, 403)
