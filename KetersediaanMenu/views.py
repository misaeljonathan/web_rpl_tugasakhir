from django.shortcuts import render
from django.http import JsonResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from TambahMenu.models import Menu
from HalamanLogin.models import Pedagang
from BukaCounter.models import Counter


# Create your views here.
response = {}

def index(request, id):
    if "username" in request.session and ownTheMenu(request.session["username"], int(id)):
        menu = Menu.objects.get(id=str(id))
        
        response["image_url"] = "/media/" + str(menu.fotoMenu)
        response["name"] = menu.namaMenu
        response["description"] = menu.deksripsiMenu
        response["price"] = 'Rp. ' + getRupiahFormat(menu.hargaMenu) + ",00"
        response["status"] = menu.status
        response["id"] = id

        html = "ketersediaan_menu/ketersediaan_menu.html"
        return render(request, html, response)
    else:
        return HttpResponseForbidden()

@csrf_exempt
def update(request, id):
    if request.method == "POST" and "username" in request.session and ownTheMenu(request.session["username"], int(id)):
        menu = Menu.objects.get(id=str(id))
        menu.status= request.POST["status"]
        menu.save()
        return JsonResponse({"message" : "success"})
    else:
        return HttpResponseForbidden()

def getRupiahFormat(number):
    number = str(number)
    if len(number) <= 3 :
        return number
    else:
        return getRupiahFormat(number[:-3]) + '.' + number[-3:]

def ownTheMenu(username, id):
    menuList = Menu.objects.filter(counter=Counter.objects.get(pedagang=Pedagang.objects.get(username=username)))
    for menu in menuList:
        if menu.id == id:
            return True
    return False