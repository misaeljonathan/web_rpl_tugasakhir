from django.shortcuts import render, redirect
from HalamanLogin.models import *
from HalamanLogin.views import validate_user
from .models import Menu
from django.http import HttpResponseForbidden, HttpResponse
from .forms import Menu_Form
from BukaCounter.models import *
# Create your views here.
response = {}

def index(request):
    if 'username' in request.session:
        response['menu_form']=Menu_Form
        clientName = request.session.get('username')
        valid = validate_user(clientName)
        return render(request, "TambahMenu/TambahMenu.html", response)
    else:
        return HttpResponseForbidden()
    
def submit_form(request):
    form = Menu_Form(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        response['namaMenu']=request.POST['namaMenu']
        response['deksripsiMenu']=request.POST['deksripsiMenu']
        response['fotoMenu']=request.FILES['fotoMenu']
        response['hargaMenu']=request.POST['hargaMenu']
        response['status']=request.POST['status']
        menu=Menu(
            namaMenu = response['namaMenu'],
            deksripsiMenu= response['deksripsiMenu'],
            fotoMenu= response['fotoMenu'],
            hargaMenu=response['hargaMenu'],
            status=response['status'],
            counter=Counter.objects.get(pedagang=Pedagang.objects.get(username=request.session["username"]))
        )
        menu.save()
        return redirect('/landing-page/daftar-menu')
    else:
        return HttpResponse(str(form.errors))