from django.test import TestCase
from django.urls import resolve
from .views import *
from HalamanLogin.models import *
from BukaCounter.models import Counter
from django.core.files.uploadedfile import SimpleUploadedFile
small_gif = (
    b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
    b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
    b'\x02\x4c\x01\x00\x3b'
)
uploaded = SimpleUploadedFile('small.jpg', small_gif, content_type='image/jpg')
class TambahMenuUnitTest(TestCase):
    def setUp(self):
        self.pedagang = Pedagang.objects.create(username="user", password="pass")
        self.counter = Counter.objects.create(nama= "asal",jenis= "3",jam_buka= "00:00",jam_tutup= "08:00",status_buka= "Buka", pedagang=self.pedagang)
        self.requestPost={
            "namaMenu":"asal",
            "deksripsiMenu" : "Makanan",
            "hargaMenu":"1000",
            "status":"ada",
            "counter":self.counter,
			"fotoMenu":uploaded,
        }
    def test_tambah_menu_url_is_exist(self):
        response = self.client.get('/tambah-menu/')
        self.assertEqual(response.status_code, 403)
        postForm = {"username":"user", "password":"pass"}
        self.client.post('/halaman-login/login/', postForm)
        response = self.client.get('/tambah-menu/')
        self.assertEqual(response.status_code, 200)
    def test_model_can_create_new_menu(self):
        new_menu = Menu.objects.create(
            namaMenu = "asal",
            deksripsiMenu = "Makanan",
            hargaMenu="1000",
            status="Ada",
            counter=self.counter,
			fotoMenu=uploaded,
        )
        counting_all_menu = Menu.objects.all().count()
        self.assertEqual(counting_all_menu, 1)

    def test_tambah_menu_post_success(self):
        postForm = {"username":"user", "password":"pass"}
        self.client.post('/halaman-login/login/', postForm)
        response_post = self.client.post('/tambah-menu/submit-form/', self.requestPost)
        self.assertEqual(response_post.status_code, 302)
        counting_all_menu = Menu.objects.all().count()
        self.assertEqual(counting_all_menu, 1)


# Create your tests here.
