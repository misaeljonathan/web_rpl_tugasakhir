from django import forms
from django.core.validators import RegexValidator
STATUS_CHOICES=(
    ('ada', 'Ada'),
	('tidak ada', 'Tidak ada'),
	('habis', 'Habis'),
)
class Menu_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    attrs = {
        'class': 'form-control'
    }

    namaMenu_attr = {
        'class': 'form-control',
        'placeholder':'Nama Menu'
    }
	
    deksripsiMenu_attr = {
        'class': 'form-control',
        'placeholder':'Deksripsi Menu'
    }
	
    fotoMenu_attr = {
        'class': 'form-control',
    }
	
    hargaMenu_attr = {
        'class': 'form-control',
        'placeholder': 'Harga Per porsi'
    }
	
    status_attr = {
        'class': 'form-control',
        'placeholder': 'Status Ketersediaan'    
    }
    alphabetonly=RegexValidator(r'^[a-zA-Z ]*$', 'Only alphabet characters are allowed.')
    namaMenu = forms.CharField(label='Nama Menu', required=True, max_length=50, widget=forms.TextInput(attrs=namaMenu_attr), validators=[alphabetonly])
    deksripsiMenu = forms.CharField(label='Deksripsi', max_length=300, widget=forms.TextInput(attrs=deksripsiMenu_attr), required=True)
    fotoMenu = forms.ImageField(label='Foto Menu', widget=forms.FileInput(attrs=fotoMenu_attr), required=False)
    hargaMenu = forms.IntegerField(label='Harga per Porsi:', required=True, widget=forms.NumberInput(attrs=hargaMenu_attr), min_value=0)
    status = forms.ChoiceField(label='Status Ketersediaan', required=True, choices=STATUS_CHOICES)