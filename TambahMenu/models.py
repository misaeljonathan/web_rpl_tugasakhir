from django.db import models
from BukaCounter.models import Counter
# Create your models here.
class Menu(models.Model):
    namaMenu = models.CharField(max_length=50)
    deksripsiMenu = models.CharField(max_length=300)
    fotoMenu = models.FileField(blank=True, null=True)
    hargaMenu = models.IntegerField(default=0)
    counter = models.ForeignKey(Counter, on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=20, default='Ada')